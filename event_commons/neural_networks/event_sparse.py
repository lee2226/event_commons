
import os
import sys

import cPickle
import numpy as np
import theano
import theano.tensor as T
import lasagne
from theano.compile.nanguardmode import NanGuardMode

def row_dot(A, B):
    C = (A * B).sum(axis=1)
    return C

class SkipGram:
    def __init__(self):
        pass

    def init_net(self, n_embed, embed_dim, batch_size, neg_ratio, learning_rate, epsilon=1e-8, momentum_rate=0.9):

        self.n_embed = n_embed
        self.embed_dim = embed_dim
        self.batch_size = batch_size
        self.neg_ratio = neg_ratio
        self.learning_rate = learning_rate
        self.epsilon = epsilon
        self.momentum_rate = momentum_rate

        print "n_embed = %d" % n_embed
        print "embed_dim = %d" % embed_dim
        print "batch_size = %d" % batch_size
        print "neg_ratio = %d" % neg_ratio
        print "learning_rate = %f" % learning_rate
        print "epsilon = %f" % epsilon
        print "momentum_rate = %f" % momentum_rate

        
        # shape = (batch_size, 3)
        pos_inputs = T.imatrix('pos_inputs')
        # shape = (batch_size * neg_ratio, 3)
        neg_inputs = T.imatrix('neg_inputs')

        combined_inputs = T.concatenate(
                [pos_inputs, neg_inputs],
                axis = 0)

        context_idxs = combined_inputs[:, 0]
        query_idxs = combined_inputs[:, 1]
        labels = combined_inputs[:, 2] # should be 1, -1

        # build input layer
        l_in_context = lasagne.layers.InputLayer(
                shape=(batch_size*(neg_ratio+1), ),
                input_var=context_idxs)
        l_in_query = lasagne.layers.InputLayer(
                shape=(batch_size*(neg_ratio+1), ),
                input_var=query_idxs)
    
        # build embedding layer
        self.l_embed_context = lasagne.layers.EmbeddingLayer(
                incoming=l_in_context,
                input_size=n_embed,
                output_size=embed_dim,
                W=lasagne.init.Normal())
        self.l_embed_query = lasagne.layers.EmbeddingLayer(
                incoming=l_in_query,
                input_size=n_embed,
                output_size=embed_dim,
                W=lasagne.init.Normal())

        context_embeddings = lasagne.layers.get_output(self.l_embed_context)
        query_embeddings = lasagne.layers.get_output(self.l_embed_query)

        v = row_dot(context_embeddings, query_embeddings)
        #v = T.diagonal(T.dot(context_embeddings * query_embeddings.T))
        P = T.nnet.sigmoid(v)

        # loss function
        pos_P = P[:batch_size]
        neg_P = P[batch_size:]

        # sigmoid(-x) == 1 - sigmoid(x)
        pos_score = T.log(pos_P + epsilon).sum()
        neg_score = T.log(1 - neg_P + epsilon).sum()
        loss = -(pos_score + neg_score) / batch_size

        # set train function
        params = lasagne.layers.get_all_params(self.l_embed_query, trainable=True) \
                + lasagne.layers.get_all_params(self.l_embed_context, trainable=True)

        # all these update algorithms end with producing the NaN
        updates = lasagne.updates.adam(
                loss,
                params)
        '''
        updates = lasagne.updates.adagrad(
                loss,
                params,
                learning_rate=self.learning_rate,
                epsilon=self.epsilon)
        '''
        '''
        updates = lasagne.updates.nesterov_momentum(
                loss,
                params,
                self.learning_rate,
                self.momentum_rate)
        '''
        
        self.train_fn = theano.function(
                inputs=[pos_inputs, neg_inputs],
                outputs=loss,
                updates=updates)
                #mode=NanGuardMode(nan_is_error=True, inf_is_error=True, big_is_error=True))


    def get_embeddings(self):
        return self.get_weights_context_layer()

    def get_weights_query_layer(self):
        return lasagne.layers.get_all_param_values(self.l_embed_query, trainable=True)[0]
    
    def get_weights_context_layer(self):
        return lasagne.layers.get_all_param_values(self.l_embed_context, trainable=True)[0]

    def calc_prob(self, query_idx, context_idx):
        # ToDo: not efficient if we get it from gpu every time, fix it
        embed_query = lasagne.layers.get_all_param_values(self.l_embed_query, trainable=True)[0]
        embed_context = lasagne.layers.get_all_param_values(self.l_embed_context, trainable=True)[0]
        dot_res = np.dot(embed_query[query_idx], embed_context[context_idx])
        p = 1 / (1 + np.exp(-dot_res))
        return p

    def calc_cost(self, query_idx, context_idx):
        p = self.calc_prob(query_idx, context_idx)
        cost = -np.log(p)
        return cost

    def dump_model(self, fpath):
        param_values = lasagne.layers.get_all_param_values(self.l_embed_query, trainable=True) \
                + lasagne.layers.get_all_param_values(self.l_embed_context, trainable=True)
        
        np.savez(
                fpath, 
                query=param_values[0],
                context=param_values[1],
                n_embed=self.n_embed,
                embed_dim=self.embed_dim,
                batch_size=self.batch_size,
                neg_ratio=self.neg_ratio,
                learning_rate=self.learning_rate,
                momentum_rate=self.momentum_rate,
                epsilon=self.epsilon)

    def load_model(self, fpath):
        model = np.load(fpath)

        self.n_embed = model['n_embed']
        self.embed_dim = model['embed_dim']
        self.batch_size = model['batch_size']
        self.neg_ratio = model['neg_ratio']
        self.learning_rate = model['learning_rate']
        self.epsilon = model['epsilon']
        self.momentum_rate = model['momentum_rate']
        
        self.init_net(self.n_embed, self.embed_dim, self.batch_size, self.neg_ratio, self.learning_rate, self.epsilon, self.momentum_rate)

        #query_shared = lasagne.layers.get_all_params(self.l_embed_query, trainable=True)
        #context_shared = lasagne.layers.get_all_params(self.l_embed_context, trainable=True)
        #query_shared.set_value(model['query'])
        #coontext_shared.set_value(model['context'])
        
        lasagne.layers.set_all_param_values(self.l_embed_query, [model['query']])
        lasagne.layers.set_all_param_values(self.l_embed_context, [model['context']])
        



