
import os
import sys
import logging
from collections import OrderedDict
import cPickle
import numpy as np
import theano
import theano.tensor as T
#import lasagne
#from theano.compile.nanguardmode import NanGuardMode

def row_dot(A, B):
    C = (A * B).sum(axis=1)
    return C

class SkipGram:
    def __init__(self):
        pass

    def init_net(self, n_embed, embed_dim, batch_size, neg_ratio, feature_dims,
            learning_rate=1.0, epsilon=1e-6):
        self.n_embed = n_embed
        self.embed_dim = embed_dim
        self.batch_size = batch_size
        self.neg_ratio = neg_ratio
        self.learning_rate = learning_rate
        self.epsilon = epsilon
        self.feature_dims = feature_dims
        logging.info("n_embed = %d" % n_embed)
        logging.info("embed_dim = %d" % embed_dim)
        logging.info("batch_size = %d" % batch_size)
        logging.info("neg_ratio = %d" % neg_ratio)
        logging.info("learning_rate = %f" % learning_rate)
        logging.info("epsilon = %f" % epsilon)
        logging.info("feature_dims = %s" % str(feature_dims))

        # (batch_size, 2)
        pos_epairs = T.imatrix('pos_epairs')
        # (batch_size*neg_ratio, 2)
        neg_epairs = T.imatrix('neg_epairs')

        n_fsets = len(feature_dims)
        # (n_fsets, batch_size, 2)
        pos_fpairs = T.itensor3("pos_fpairs")
        # (n_fsets, batch_size, 2)
        neg_fpairs = T.itensor3("neg_fpairs")

        # (n_embed, embed_dim)
        # W_in is randomlyl initialized with the values [-0.5/m, 0.5/m]
        init_array = np.random.uniform(low=-0.5/embed_dim,
                                        high=0.5/embed_dim,
                                        size=(n_embed, embed_dim))
        self.W_in_e1 = theano.shared(np.asarray(init_array, dtype=theano.config.floatX),
                                    name='W_in_e1',
                                    borrow=True)
        # (n_embed, embed_dim)
        self.W_in_e2 = theano.shared(np.asarray(init_array, dtype=theano.config.floatX),
                                    name='W_in_e2',
                                    borrow=True)
        # W_out_events is initialized as zero arrays
        # (n_embed, embed_dim)
        self.W_out_events = theano.shared(np.zeros((n_embed, embed_dim), dtype=theano.config.floatX),
                                    name='W_out_events',
                                    borrow=True)

        # feature parts
        self.W_in_features = []
        self.W_out_features = []
        for i, fdim in enumerate(feature_dims):
            # W_in_features is randomlyl initialized with the values [-0.5/m, 0.5/m]
            # (feature_dim, embed_dim)
            init_array = np.random.uniform(low=-0.5/embed_dim,
                                            high=0.5/embed_dim,
                                            size=(fdim, embed_dim))
            self.W_in_features.append(theano.shared(np.asarray(init_array, dtype=theano.config.floatX),
                                                name='W_in_features_%d' % i,
                                                borrow=True))
            # W_out_features is initialized as zero arrays
            self.W_out_features.append(theano.shared(np.zeros((fdim, embed_dim), dtype=theano.config.floatX),
                                                name='W_out_features_%d' % i,
                                                borrow=True))

        # (batch_size,)
        pos_context_idxs = pos_epairs[:, 0]
        pos_query_idxs = pos_epairs[:, 1]
        # (batch_size*neg_ratio,)
        neg_context_idxs = neg_epairs[:, 0]
        neg_query_idxs = neg_epairs[:, 1]

        # (batch_size, embed_dim)
        V_pos_in_e1 = self.W_in_e1[pos_context_idxs]
        V_pos_in_e2 = self.W_in_e2[pos_query_idxs]
        V_pos_out = self.W_out_events[pos_query_idxs]
        # (batch_size*neg_ratio, embed_dim)
        V_neg_in_e1 = self.W_in_e1[neg_context_idxs]
        V_neg_out = self.W_out_events[neg_query_idxs]
        ## interleaving order
        ## (batch_size*neg_ratio, embed_dim)
        V_neg_out_interleaving = V_neg_out[0::neg_ratio]
        for i in range(1, neg_ratio):
            V_neg_out_interleaving = T.concatenate(
                    [V_neg_out_interleaving, V_neg_out[i::neg_ratio]],
                    axis=0)
        
        def sum_features(fpairs):
            for i, fdim in enumerate(self.feature_dims):
                # (batch_size, )
                fset_ctx_fidxs = fpairs[i, :, 0]
                if i == 0:
                    # (batch_size, emb_dim)
                    h_features = self.W_in_features[i][fset_ctx_fidxs]
                else:
                    h_features = h_features + self.W_in_features[i][fset_ctx_fidxs]
            return h_features

        # sum feature parts
        # (batch_size, emb_dim)
        h_pos_features = sum_features(pos_fpairs)
        h_neg_features = sum_features(neg_fpairs)

        # (n_fsets, batch_size)
        pos_out_fidxs = pos_fpairs[:, :, 1]
        neg_out_fidxs = neg_fpairs[:, :, 1]

        # get losses
        loss_pos_events, loss_neg_events = self.event_network(h_pos_features,
                                                                V_pos_in_e1, V_neg_in_e1,
                                                                V_pos_out, V_neg_out_interleaving,
                                                                neg_ratio=neg_ratio, epsilon=epsilon)
        loss_pos_features, loss_neg_features = self.feature_network(h_pos_features, h_neg_features,
                                                                V_pos_in_e1, V_pos_in_e2,
                                                                pos_out_fidxs, neg_out_fidxs,
                                                                epsilon=epsilon)

        loss = 0.1*loss_pos_events + 0.1*loss_neg_events + 0.1*loss_pos_features + 1.0*loss_neg_features
        params = [self.W_in_e1, self.W_in_e2, self.W_out_events] + self.W_in_features + self.W_out_features
        updates = self.adagrad(params, loss, epsilon=self.epsilon, learning_rate=learning_rate)

        # compile functions
        self.train_fn = theano.function(inputs=[pos_epairs, neg_epairs, pos_fpairs, neg_fpairs],
                                        outputs=loss,
                                        updates=updates)
        
        self.embedding_fn = theano.function(inputs=[pos_epairs, pos_fpairs],
                                            outputs=self.h_pos_1)

        self.prob_fn = theano.function(inputs=[pos_epairs, pos_fpairs],
                                       outputs=self.out_pos_1)

    def get_prob(self, pos_epairs, pos_fpairs):
        return self.prob_fn(pos_epairs, pos_fpairs)

    def get_embeddings(self, pos_epairs, pos_fpairs):
        return self.embedding_fn(pos_epairs, pos_fpairs)

    def feature_network(self, h_pos_features, h_neg_features, V_pos_in_e1, V_pos_in_e2, pos_out_fidxs, neg_out_fidxs, epsilon):
        ############### Network2: Input -> Hidden #################
        # positive samples
        # (batch_size, emb_dim)
        h_pos_2 = h_pos_features + V_pos_in_e1 + V_pos_in_e2
        h_neg_2 = h_neg_features + V_pos_in_e1 + V_pos_in_e2

        ############### Network2: Hidden -> Output #################
        for i, fdim in enumerate(self.feature_dims):
            # sigmoid(row_dot((batch_size, emb_dim), (batch_size, emb_dim)))
            # (batch_size, )
            out_pos_2 = T.nnet.sigmoid(row_dot(h_pos_2, self.W_out_features[i][pos_out_fidxs[i]]))
            loss_pos_2 = -(T.log(out_pos_2 + epsilon).sum())
            acc_loss_pos_2 = loss_pos_2 if i == 0 else acc_loss_pos_2 + loss_pos_2
        
            out_neg_2 = T.nnet.sigmoid(-1 * row_dot(h_neg_2, self.W_out_features[i][neg_out_fidxs[i]]))
            loss_neg_2 = -(T.log(out_neg_2 + epsilon).sum())
            acc_loss_neg_2 = loss_neg_2 if i == 0 else acc_loss_neg_2 + loss_neg_2
        
        return acc_loss_pos_2, acc_loss_neg_2

    def event_network(self, h_pos_features, V_pos_in_e1, V_neg_in_e1, V_pos_out, V_neg_out_interleaving, neg_ratio, epsilon):
        ############### Network1: Input -> Hidden #################
        # positive samples
        # (batch_size, emb_dim)
        self.h_pos_1 = h_pos_features + V_pos_in_e1

        # negative samples
        # interleaving order
        # note that we use positive features here
        # (batch_size*neg_ratio, emb_dim)
        h_neg_1 = h_pos_features + V_neg_in_e1[0::neg_ratio]
        for i in range(1, neg_ratio):
            h_neg_1 = T.concatenate([h_neg_1, h_pos_features + V_neg_in_e1[i::neg_ratio]],
                                    axis=0)

        ############### Network1: Hidden -> Output #################
        # positive samples
        # sigmoid(row_dot((batch_size, embed_dim), (batch_size, embed_dim)))
        # (batch_size, )
        self.out_pos_1 = T.nnet.sigmoid(row_dot(self.h_pos_1, V_pos_out))
        loss_pos_1 = -(T.log(self.out_pos_1 + epsilon).sum())

        # negative samples
        ### sigmoid(-row_dot(batch_size*neg_ratio, emb_dim), (batch_size*neg_ratio, embed_dim))
        ### (batch_size*neg_ratio, )
        out_neg_1 = T.nnet.sigmoid(-1 * row_dot(h_neg_1, V_neg_out_interleaving))
        loss_neg_1 = -(T.log(out_neg_1 + epsilon).sum())

        return loss_pos_1, loss_neg_1

    def adagrad(self, params, loss, epsilon, learning_rate):
        # adagrad (porting from lasagne)
        grads = T.grad(loss, params)
        updates = OrderedDict()
        for param, grad in zip(params, grads):
            value = param.get_value(borrow=True)
            accu = theano.shared(np.zeros(value.shape, dtype=value.dtype),
                                    broadcastable=param.broadcastable)
            accu_new = accu + grad ** 2
            updates[accu] = accu_new
            updates[param] = param - (learning_rate * grad / T.sqrt(accu_new + epsilon))
        
        return updates

    def dump_model(self, fpath):
        l_W_in_features = [f.get_value() for f in self.W_in_features]
        l_W_out_features = [f.get_value() for f in self.W_out_features]
        np.savez(fpath,
                 W_in_e1=self.W_in_e1.get_value(),
                 W_in_e2=self.W_in_e2.get_value(),
                 W_out_events=self.W_out_events.get_value(),
                 l_W_in_features=l_W_in_features,
                 l_W_out_features=l_W_out_features,
                 n_embed=self.n_embed,
                 embed_dim=self.embed_dim,
                 batch_size=self.batch_size,
                 neg_ratio=self.neg_ratio,
                 learning_rate=self.learning_rate,
                 epsilon=self.epsilon,
                 feature_dims=self.feature_dims)

    def load_model(self, fpath):
        model = np.load(fpath)
        self.n_embed = int(model['n_embed'])
        self.embed_dim = int(model['embed_dim'])
        self.batch_size = int(model['batch_size'])
        self.neg_ratio = int(model['neg_ratio'])
        self.learning_rate = float(model['learning_rate'])
        self.epsilon = float(model['epsilon'])
        self.feature_dims = model['feature_dims'].tolist()
        self.init_net(self.n_embed, self.embed_dim, self.batch_size, self.neg_ratio, self.feature_dims, self.learning_rate, self.epsilon)

        keys = ["W_in_e1", "W_in_e2", "W_out_events"]
        params = [self.W_in_e1, self.W_in_e2, self.W_out_events]
        for param, key in zip(params, keys):
            param.set_value(model[key])

        keys = ["l_W_in_features", "l_W_out_features"]
        param_lists = [self.W_in_features, self.W_out_features]
        for param_list, key in zip(param_lists, keys):
            vlist = model[key]
            for param, v in zip(param_list, vlist):
                param.set_value(v)

