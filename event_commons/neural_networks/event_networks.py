
import os
import sys
import logging
import collections

import theano
import theano.tensor as T
import lasagne
import numpy as np

from event_commons import utils
from network_base import NetworkBase

class NetworkEventConcatenate(NetworkBase):
    def __init__(
            self,
            input_var1,
            input_var2,
            target_var,
            input_dim1,
            input_dim2,
            batch_size=None,
            n_classes=3,
            n_l1_hidden=100,
            n_l2_hidden=100,
            p_in_dropout=0.5,
            p_h_dropout=0.2,
            weights_per_label=None):
        """Create concatenated event network

        Args:
            p_in_dropout: dropout rate of the input layer. None, if it is not applied.
            p_h_dropout: dropout rate of the hidden layers. None, if it is not applied.

        """
        super(NetworkEventConcatenate, self).__init__()

        # setup network
        # input layer
        self.l_in1 = lasagne.layers.InputLayer(
                shape=(batch_size, input_dim1),
                input_var=input_var1)
        
        self.l_in2 = lasagne.layers.InputLayer(
                shape=(batch_size, input_dim2),
                input_var=input_var2)

        # input dropout
        if p_in_dropout is None:
            self.l_in1_drop = self.l_in1
            self.l_in2_drop = self.l_in2
        else:
            self.l_in1_drop = lasagne.layers.DropoutLayer(self.l_in1, p=p_in_dropout)
            self.l_in2_drop = lasagne.layers.DropoutLayer(self.l_in2, p=p_in_dropout)
        
        # first hidden layer
        self.l_h1_1 = lasagne.layers.DenseLayer(
                self.l_in1_drop,
                num_units=n_l1_hidden,
                nonlinearity=lasagne.nonlinearities.rectify)
    
        self.l_h1_2 = lasagne.layers.DenseLayer(
                self.l_in2_drop,
                num_units=n_l1_hidden,
                nonlinearity=lasagne.nonlinearities.rectify)

        # first hidden layer dropout
        if p_h_dropout is None:
            self.l_h1_1_drop = self.l_h1_1
            self.l_h1_2_drop = self.l_h1_2
        else:
            self.l_h1_1_drop = lasagne.layers.DropoutLayer(
                    self.l_h1_1,
                    p=p_h_dropout)
            self.l_h1_2_drop = lasagne.layers.DropoutLayer(
                    self.l_h1_2,
                    p=p_h_dropout)

        # concatenate
        self.l_h1_concat = lasagne.layers.ConcatLayer((self.l_h1_1_drop, self.l_h1_2_drop))
        
        # second hiddent layer
        self.l_h2 = lasagne.layers.DenseLayer(
                self.l_h1_concat,
                num_units=n_l2_hidden,
                nonlinearity=lasagne.nonlinearities.rectify)

        if p_h_dropout is None:
            self.l_h2_drop = self.l_h2
        else:
            self.l_h2_drop = lasagne.layers.DropoutLayer(
                    self.l_h2,
                    p=p_h_dropout)

        # output
        self.l_out = lasagne.layers.DenseLayer(
                self.l_h2_drop,
                num_units=n_classes,
                nonlinearity=lasagne.nonlinearities.softmax)

        self.basic_functions(input_var1, input_var2, target_var, weights_per_label)
       

class NetworkTwoHidden(NetworkBase):
    def __init__(
            self,
            input_var1,
            input_var2,
            target_var,
            input_dim1,
            input_dim2,
            batch_size=None,
            n_classes=3,
            n_l1_hidden=100,
            n_l2_hidden=100,
            p_in_dropout=0.5,
            p_h_dropout=0.2,
            weights_per_label=None):
        """Create a network with two hidden layers

        Args:
            p_in_dropout: dropout rate of the input layer. None, if it is not applied.
            p_h_dropout: dropout rate of the hidden layers. None, if it is not applied.

        """
        super(NetworkTwoHidden, self).__init__()

        # setup network
        input_dim = input_dim1 + input_dim2
        all_inputs = T.concatenate([input_var1, input_var2], axis=1)

        # input layer
        self.l_in = lasagne.layers.InputLayer(
                shape=(batch_size, input_dim),
                input_var=all_inputs)
        
        # input dropout
        if p_in_dropout is None:
            self.l_in_drop = self.l_in
        else:
            self.l_in_drop = lasagne.layers.DropoutLayer(self.l_in, p=p_in_dropout)
        
        # first hidden layer
        self.l_h1 = lasagne.layers.DenseLayer(
                self.l_in_drop,
                num_units=n_l1_hidden,
                nonlinearity=lasagne.nonlinearities.rectify)
    
        # first hidden layer dropout
        if p_h_dropout is None:
            self.l_h1_drop = self.l_h1
        else:
            self.l_h1_drop = lasagne.layers.DropoutLayer(
                    self.l_h1,
                    p=p_h_dropout)

        # second hiddent layer
        self.l_h2 = lasagne.layers.DenseLayer(
                self.l_h1_drop,
                num_units=n_l2_hidden,
                nonlinearity=lasagne.nonlinearities.rectify)

        if p_h_dropout is None:
            self.l_h2_drop = self.l_h2
        else:
            self.l_h2_drop = lasagne.layers.DropoutLayer(
                    self.l_h2,
                    p=p_h_dropout)

        # output
        self.l_out = lasagne.layers.DenseLayer(
                self.l_h2_drop,
                num_units=n_classes,
                nonlinearity=lasagne.nonlinearities.softmax)

        self.basic_functions(input_var1, input_var2, target_var, weights_per_label)


class NetworkCSignals(NetworkBase):
    def __init__(
            self,
            input_var1,
            input_var2,
            input_var3,
            target_var,
            input_dim1,
            input_dim2,
            input_dim3,
            batch_size=None,
            n_classes=3,
            n_l1_hidden=100,
            n_l2_hidden=100,
            p_in_dropout=0.5,
            p_h_dropout=0.2,
            weights_per_label=None):
        """Create concatenated event network

        Args:
            p_in_dropout: dropout rate of the input layer. None, if it is not applied.
            p_h_dropout: dropout rate of the hidden layers. None, if it is not applied.

        """
        super(NetworkCSignals, self).__init__()

        # input layer
        self.l_in1 = lasagne.layers.InputLayer(
                shape=(batch_size, input_dim1),
                input_var=input_var1)
        
        self.l_in2 = lasagne.layers.InputLayer(
                shape=(batch_size, input_dim2),
                input_var=input_var2)
        
        self.l_in3 = lasagne.layers.InputLayer(
                shape=(batch_size, input_dim3),
                input_var=input_var3)

        # input dropout
        if p_in_dropout is None:
            self.l_in1_drop = self.l_in1
            self.l_in2_drop = self.l_in2
            self.l_in3_drop = self.l_in3
        else:
            self.l_in1_drop = lasagne.layers.DropoutLayer(self.l_in1, p=p_in_dropout)
            self.l_in2_drop = lasagne.layers.DropoutLayer(self.l_in2, p=p_in_dropout)
            self.l_in3_drop = lasagne.layers.DropoutLayer(self.l_in3, p=p_in_dropout)
        
        # first hidden layer
        self.l_h1_1 = lasagne.layers.DenseLayer(
                self.l_in1_drop,
                num_units=n_l1_hidden,
                nonlinearity=lasagne.nonlinearities.rectify)
    
        self.l_h1_2 = lasagne.layers.DenseLayer(
                self.l_in2_drop,
                num_units=n_l1_hidden,
                nonlinearity=lasagne.nonlinearities.rectify)

        self.l_h1_3 = lasagne.layers.DenseLayer(
                self.l_in3_drop,
                num_units=n_l1_hidden,
                nonlinearity=lasagne.nonlinearities.rectify)
        
        # first hidden layer dropout
        if p_h_dropout is None:
            self.l_h1_1_drop = self.l_h1_1
            self.l_h1_2_drop = self.l_h1_2
            self.l_h1_3_drop = self.l_h1_3
        else:
            self.l_h1_1_drop = lasagne.layers.DropoutLayer(
                    self.l_h1_1,
                    p=p_h_dropout)
            self.l_h1_2_drop = lasagne.layers.DropoutLayer(
                    self.l_h1_2,
                    p=p_h_dropout)
            self.l_h1_3_drop = lasagne.layers.DropoutLayer(
                    self.l_h1_3,
                    p=p_h_dropout)

        # concatenate
        self.l_h1_concat = lasagne.layers.ConcatLayer((self.l_h1_1_drop, self.l_h1_2_drop, self.l_h1_3_drop))
        
        # second hiddent layer
        self.l_h2 = lasagne.layers.DenseLayer(
                self.l_h1_concat,
                num_units=n_l2_hidden,
                nonlinearity=lasagne.nonlinearities.rectify)

        if p_h_dropout is None:
            self.l_h2_drop = self.l_h2
        else:
            self.l_h2_drop = lasagne.layers.DropoutLayer(
                    self.l_h2,
                    p=p_h_dropout)

        # output
        self.l_out = lasagne.layers.DenseLayer(
                self.l_h2_drop,
                num_units=n_classes,
                nonlinearity=lasagne.nonlinearities.softmax)

        # training functions
        prediction = lasagne.layers.get_output(self.l_out)
        #loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
        loss = NetworkBase.weighted_crossentropy(prediction, target_var, weights_per_label)
        loss = loss.mean()
        params = lasagne.layers.get_all_params(self.l_out, trainable=True)
        updates = lasagne.updates.adam(loss, params)
        #updates = lasagne.updates.nesterov_momentum(loss, params, learning_rate=0.01, momentum=0.9)
        self.train_fn = theano.function([input_var1, input_var2, input_var3, target_var], loss, updates=updates)

        # testing functions
        test_prediction = lasagne.layers.get_output(self.l_out, deterministic=True)
        #test_loss = lasagne.objectives.categorical_crossentropy(test_prediction, target_var)
        test_loss = NetworkBase.weighted_crossentropy(test_prediction, target_var, weights_per_label)
        test_loss = test_loss.mean()
        test_acc = T.mean(
                T.eq(T.argmax(test_prediction, axis=1), target_var),
                dtype=theano.config.floatX)
        test_predict_label = T.argmax(test_prediction, axis=1)
        self.val_fn = theano.function([input_var1, input_var2, input_var3, target_var], [test_loss, test_acc])
        self.predict_fn = theano.function([input_var1, input_var2, input_var3], test_predict_label)

