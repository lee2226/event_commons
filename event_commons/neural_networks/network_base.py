
import os
import sys
import logging

import theano
import theano.tensor as T
import lasagne

class NetworkBase(object):
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def weighted_crossentropy(predictions, targets, weights_per_label):
        if weights_per_label is not None:
            weights_per_label = theano.shared(lasagne.utils.floatX(weights_per_label))
            weights = weights_per_label[targets]  #returns a targets-shaped weight matrix
            loss = lasagne.objectives.aggregate(T.nnet.categorical_crossentropy(predictions, targets), weights=weights)
        else:
            loss = lasagne.objectives.categorical_crossentropy(predictions, targets)
        return loss
    
    def basic_functions(self, input_var1, input_var2, target_var, weights_per_label):
        # training functions
        prediction = lasagne.layers.get_output(self.l_out)
        #loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
        loss = NetworkBase.weighted_crossentropy(prediction, target_var, weights_per_label)
        loss = loss.mean()
        params = lasagne.layers.get_all_params(self.l_out, trainable=True)
        updates = lasagne.updates.adam(loss, params)
        #updates = lasagne.updates.nesterov_momentum(loss, params, learning_rate=0.01, momentum=0.9)
        self.train_fn = theano.function([input_var1, input_var2, target_var], loss, updates=updates)

        # testing functions
        test_prediction = lasagne.layers.get_output(self.l_out, deterministic=True)
        #test_loss = lasagne.objectives.categorical_crossentropy(test_prediction, target_var)
        test_loss = NetworkBase.weighted_crossentropy(test_prediction, target_var, weights_per_label)
        test_loss = test_loss.mean()
        test_acc = T.mean(
                T.eq(T.argmax(test_prediction, axis=1), target_var),
                dtype=theano.config.floatX)
        test_predict_label = T.argmax(test_prediction, axis=1)
        self.val_fn = theano.function([input_var1, input_var2, target_var], [test_loss, test_acc])
        self.predict_fn = theano.function([input_var1, input_var2], test_predict_label)


