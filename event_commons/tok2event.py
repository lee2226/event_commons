
import os
import sys
import logging
import argparse

import animacy
from event_commons import extract_events

count_unknown_ani = 0
count_known_ani = 0

count_no_estrs = 0
count_verb_no_estrs = 0
count_adj_no_estrs = 0
count_yes_estrs = 0

count_pos_error = 0
count_pos_noerror = 0
count_pos_notfound = 0

def extract_estrs(doc_id, sent_idx, tok_idx, tok, parsed):
    global count_unknown_ani
    global count_known_ani
    estrs = []
    sent_deps = parsed[doc_id]['sentences'][sent_idx]['collapsed-ccprocessed-dependencies']
    sent_toks = parsed[doc_id]['sentences'][sent_idx]['tokens']
    key_deps = extract_events.find_outgoing_deps(tok, tok_idx+1, sent_deps, extract_events.target_deps)
    for key_dep in key_deps:
        entity = key_dep['dependentGloss']
        ent_tok_idx = key_dep['dependent'] - 1
        ani = animacy.get_animacy_from_parsed(parsed, doc_id, sent_idx, ent_tok_idx)
        if ani is None:
            ani = animacy.get_animacy(sent_toks[ent_tok_idx]['word'], sent_toks[ent_tok_idx]['ner'])
        if ani == animacy.UNKNOWN_ANIMACY_STR:
            count_unknown_ani += 1
        else:
            count_known_ani += 1
        dep = key_dep['dep']
        predicate = extract_events.get_predicate(key_dep, sent_deps, False)
        estr = '(%s,%s,%s)' % (predicate, dep, ani)
        #import pdb; pdb.set_trace()
        estrs.append(estr.lower())
        logging.debug('%d / %d unknown_animacy' % (count_unknown_ani, count_unknown_ani + count_known_ani))
    return estrs

def extract_for_verb(doc_id, sent_idx, tok_idx, tok, eid, parsed, corpus):
    sent_toks = parsed[doc_id]['sentences'][sent_idx]['tokens']

    global count_pos_error
    global count_pos_noerror
    global count_pos_notfound

    if eid is not None:
        if eid not in corpus.eid2pos[doc_id]:
            count_pos_notfound += 1
        elif corpus.eid2pos[doc_id][eid] != 'VERB':
            count_pos_error += 1
        else:
            count_pos_noerror += 1
    
    logging.debug('count_pos_error = %d' % (count_pos_error))
    logging.debug('count_pos_noerror = %d' % (count_pos_noerror))
    logging.debug('count_pos_notfound = %d' % (count_pos_notfound))
    return extract_estrs(doc_id, sent_idx, tok_idx, tok, parsed)

def extract_for_adj(doc_id, sent_idx, tok_idx, tok, eid, parsed, corpus):
    sent_toks = parsed[doc_id]['sentences'][sent_idx]['tokens']
    
    global count_pos_error
    global count_pos_noerror
    global count_pos_notfound
   
    if eid is not None:
        if eid not in corpus.eid2pos[doc_id]:
            count_pos_notfound += 1
        elif corpus.eid2pos[doc_id][eid] != 'ADJECTIVE':
            count_pos_error += 1
        else:
            count_pos_noerror += 1
    
    logging.debug('count_pos_error = %d' % (count_pos_error))
    logging.debug('count_pos_noerror = %d' % (count_pos_noerror))
    logging.debug('count_pos_notfound = %d' % (count_pos_notfound))
    return extract_estrs(doc_id, sent_idx, tok_idx, tok, parsed)

def extract_for_others(doc_id, sent_idx, tok_idx, tok, eid, parsed, corpus):
    """Extract events from all dependencies within two hops to the event word.


    Returns:
        A list of event strings. If the events can be found in one hop, 
        this function returns them. Otherwise, we try to find the events in two hops.

    """
    global count_unknown_ani
    global count_known_ani
    
    sent_deps = parsed[doc_id]['sentences'][sent_idx]['collapsed-ccprocessed-dependencies']
    sent_toks = parsed[doc_id]['sentences'][sent_idx]['tokens']
   
    # one hop
    incoming_edges = extract_events.find_incoming_deps(tok, tok_idx+1, sent_deps)
    key_deps = incoming_edges

    estrs = []
    for dep in key_deps:
        tmp_tok_idx = dep['governor'] - 1
        tmp_tok = dep['governorGloss']
        if extract_events.is_verb(tmp_tok, tmp_tok_idx+1, sent_toks):
            estrs += extract_for_verb(doc_id, sent_idx, tmp_tok_idx, tmp_tok, None, parsed, corpus)
        elif extract_events.is_adj(tmp_tok, tmp_tok_idx+1, sent_toks):
            estrs += extract_for_adj(doc_id, sent_idx, tmp_tok_idx, tmp_tok, None, parsed, corpus)
    
    if len(estrs) == 0:
        # two hops
        key_deps = []
        for dep in incoming_edges:
            tmp_tok_idx = dep['governor'] - 1
            tmp_tok = dep['governorGloss']
            incoming_edges2 = extract_events.find_incoming_deps(tmp_tok, tmp_tok_idx+1, sent_deps)
            key_deps += incoming_edges2 

        # extract events for all edges in two hops
        for dep in key_deps:
            tmp_tok_idx = dep['governor'] - 1
            tmp_tok = dep['governorGloss']
            if extract_events.is_verb(tmp_tok, tmp_tok_idx+1, sent_toks):
                estrs += extract_for_verb(doc_id, sent_idx, tmp_tok_idx, tmp_tok, None, parsed, corpus)
            elif extract_events.is_adj(tmp_tok, tmp_tok_idx+1, sent_toks):
                estrs += extract_for_adj(doc_id, sent_idx, tmp_tok_idx, tmp_tok, None, parsed, corpus)

    return estrs

def find_event_strings(emap, doc_id, eid, parsed, corpus):
    """

    Args:
            emap[fprefix][eid]['toks']
            emap[fprefix][eid]['sent_idx']
            emap[fprefix][eid]['tok_idxs']
    Returns:
            a list of possible event strings
    """
    global count_no_estrs
    global count_yes_estrs
    global count_verb_no_estrs
    global count_adj_no_estrs
    
    event_words = emap[doc_id][eid]['toks']
    sent_idx = emap[doc_id][eid]['sent_idx']
    tok_idxs = emap[doc_id][eid]['tok_idxs']
    tok_idx = tok_idxs[-1] # always use the last token as the predicate
    tok = event_words.split(' ')[-1]
    sent_toks = parsed[doc_id]['sentences'][sent_idx]['tokens']

    if extract_events.is_verb(tok, tok_idx+1, sent_toks):
        estrs = extract_for_verb(doc_id, sent_idx, tok_idx, tok, eid, parsed, corpus)
        if len(estrs) == 0:
            count_verb_no_estrs += 1
    elif extract_events.is_adj(tok, tok_idx+1, sent_toks):
        estrs = extract_for_adj(doc_id, sent_idx, tok_idx, tok, eid, parsed, corpus)
        if len(estrs) == 0:
            count_adj_no_estrs += 1
    else:
        estrs = extract_for_others(doc_id, sent_idx, tok_idx, tok, eid, parsed, corpus)

    if len(estrs) == 0:
        count_no_estrs += 1
    else:
        count_yes_estrs += 1

    logging.debug('count_verb_no_estrs = %d' % count_verb_no_estrs)
    logging.debug('count_adj_no_estrs = %d' % count_adj_no_estrs)
    
    logging.debug('count_no_estrs = %d' % count_no_estrs)
    logging.debug('count_yes_estrs = %d' % count_yes_estrs)

    return estrs

