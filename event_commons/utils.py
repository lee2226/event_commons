
import os
import sys
import json
import struct
import time
import re
import numpy as np

def create_dir_if_needed(_dir):
    if not os.path.exists(_dir):
        os.makedirs(_dir)

def event_to_estr(e, use_animacy=True, use_sentiment=False):
    if use_animacy and use_sentiment:
        event_string = "(%s,%s,%s,%s)" % (e['predicate'], e['dep'], e['animacy'], e['sentiment'])
    elif use_animacy:
        event_string = "(%s,%s,%s)" % (e['predicate'], e['dep'], e['animacy'])
    else:
        event_string = "(%s,%s)" % (e['predicate'], e['dep'])
    return event_string

def default_prefix(fname):
    return os.path.splitext(fname)[0]

def natural_sort(alist):
    return sorted(alist, key=lambda x: int(re.findall(r'\d+$', x)[0]))

def cosine_similarity(v1, v2):
    return np.dot(v1, v2) / (np.sqrt(np.dot(v1, v1)) * np.sqrt(np.dot(v2, v2)))

def precision(tp, fp):
    return float(tp) / (tp + fp)

def recall(tp, fn):
    return float(tp) / (tp + fn)

def f1(pr, rec):
    return 2.0*pr*rec / (pr + rec)

def normalize(alist):
    if isinstance(alist, list):
        alist = np.array(alist)
    s = sum(alist)
    return alist / float(s)

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    return sorted(l, key = alphanum_key)

