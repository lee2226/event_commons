import re


# constants
PRED_OOV = '__PRED_OOV__'
NO_ARG = '__NO_ARG__'
UNKNOWN_ARG_WORD = '__UNK__'


class Event(object):
    def __init__(self, pred, arg0, arg0_head, arg1, arg1_head, arg2, arg2_head,
                    sentiment, ani0, ani1, ani2):
        rep = {'\n': ' ', '::': ' '}
        rep = dict((re.escape(k), v) for k, v in rep.iteritems())
        pat = re.compile("|".join(rep.keys()))

        self.pred = pred.encode('ascii', 'ignore')
        self.arg0 = pat.sub(lambda m: rep[re.escape(m.group(0))], arg0).encode('ascii', 'ignore')
        self.arg0_head = arg0_head.encode('ascii', 'ignore')
        self.arg1 = pat.sub(lambda m: rep[re.escape(m.group(0))], arg1).encode('ascii', 'ignore')
        self.arg1_head = arg1_head.encode('ascii', 'ignore')
        self.arg2 = pat.sub(lambda m: rep[re.escape(m.group(0))], arg2).encode('ascii', 'ignore')
        self.arg2_head = arg2_head.encode('ascii', 'ignore')
        self.sentiment = sentiment
        self.ani0 = ani0
        self.ani1 = ani1
        self.ani2 = ani2

    def __repr__(self):
        return "({}::{}::{}::{}::{}::{}::{}::{}::{}::{}::{})".format(
                self.pred,
                self.arg0, self.arg0_head,
                self.arg1, self.arg1_head,
                self.arg2, self.arg2_head,
                self.sentiment, self.ani0,
                self.ani1, self.ani2)

    @classmethod
    def from_string(cls, line):
        line = line.rstrip("\n")[1:-1]
        sp = line.split('::')
        obj = cls(sp[0], sp[1], sp[2], sp[3], sp[4], sp[5], sp[6],
                    sp[7], sp[8], sp[9], sp[10])
        return obj

    @classmethod
    def from_json(cls, e):
        pred = e['predicate']
        # only use the first sub-argument for now
        arg0_head = e['arg0'][0] if 'arg0' in e else NO_ARG
        arg0 = e['arg0_text'][0] if 'arg0_text' in e else NO_ARG

        arg1_head = e['arg1'][0] if 'arg1' in e else NO_ARG
        arg1 = e['arg1_text'][0] if 'arg1_text' in e else NO_ARG

        arg2_head = e['arg2'][0] if 'arg2' in e else NO_ARG
        arg2 = e['arg2_text'][0] if 'arg2_text' in e else NO_ARG
        sentiment = e['sentiment'] if 'sentiment' in e else None
        ani0 = e['ani0'][0] if 'ani0' in e else animacy.UNKNOWN_ANIMACY
        ani1 = e['ani1'][0] if 'ani1' in e else animacy.UNKNOWN_ANIMACY
        ani2 = e['ani2'][0] if 'ani2' in e else animacy.UNKNOWN_ANIMACY
        obj = cls(pred, arg0, arg0_head, arg1, arg1_head, arg2, arg2_head,
                    sentiment, ani0, ani1, ani2)
        return obj

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__repr__() == other.__repr__()
        return False

    def __ne__(self, other):
        return not self.__eq__(other)
