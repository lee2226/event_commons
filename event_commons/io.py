
import os
import sys
import json
import struct
import time
import logging

import numpy as np

from . import utils

UNKNOWN_EVENT_STR = '(UNKNOWNEVENT)'

def get_unknown_event_str():
    return UNKNOWN_EVENT_STR

def get_unknown_event_embedding(dim):
    return np.random.uniform(low=-0.5/dim, high=0.5/dim, size=dim)

def load_embeddings(fpath, file_format='bin', add_unknown=True):
    embeddings = {}
    
    if file_format.startswith('.'):
        file_format = file_format[1:]
    
    form = 'rb' if file_format == 'bin' else 'r'
    with open(fpath, form) as fr:
        first_line = fr.readline().rstrip('\n')
        n_events, embed_dim = first_line.split(' ')
        n_events = int(n_events)
        embed_dim = int(embed_dim)

        if file_format == 'bin':
            for i in range(n_events):
                if i % 10000 == 0:
                    logging.info('%d / %d vectors loaded' % (i, n_events))
                estr = ''
                while True:
                    c = fr.read(1)
                    if c == ' ':
                        break
                    estr += c

                emb_bytes = fr.read(4*embed_dim)
                if fr.read(1) != '\n':
                    import pdb; pdb.set_trace()
                assert fr.read(1) == '\n'

                emb = np.zeros(embed_dim)
                for j in range(embed_dim):
                    emb[j] = struct.unpack('f', emb_bytes[4*j:4*j+4])[0]
                embeddings[estr] = emb
        else: # txt
            for i in range(n_events):
                line = fr.readline().rstrip('\n')
                line_split = line.split(' ')
                emb = np.zeros(embed_dim)
                for j in range(embed_dim):
                    emb[j] = float(line_split[1+j])
                embeddings[line_split[0]] = emb
    if add_unknown:
        embeddings[UNKNOWN_EVENT_STR] = get_unknown_event_embedding(embed_dim)
    return embeddings

def dump_embeddings(fpath, all_embeddings, id2event, embed_dim, file_format='bin'):
    
    if file_format.startswith('.'):
        file_format = file_format[1:]
    n_event = len(id2event)
    form = 'wb' if foramt == 'bin' else 'w'
    with open(fpath, form) as fw:
        fw.write('%s %s\n' % (n_event, embed_dim))

        if file_format == 'bin':
            for i in range(n_event):
                fw.write(id2event['%d' % i] + ' ')
                for j in range(embed_dim):
                    fw.write(struct.pack('f', all_embeddings[i][j]))
                fw.write('\n')
        else: # txt
            for i in range(n_event):
                fw.write(id2event['%d' % i])
                for j in range(embed_dim):
                    fw.write(' %f' % all_embeddings[i][j])
                fw.write('\n')
'''
    Json Files
'''

def load_jsons(input_folder, doc_id_fn=utils.default_prefix):
    json_files = [f for f in os.listdir(input_folder) if os.path.isfile(os.path.join(input_folder, f))]
    n_json_files = len(json_files)
    docs = {}
    count = 0
    for json_file in json_files:
        jpath = os.path.join(input_folder, json_file)
        if count % 1000 == 0:
            logging.info('%d / %d docs loaded' % (count, n_json_files))
        with open(jpath, 'r') as fr:
            doc = json.load(fr)

        doc_id = doc_id_fn(json_file)
        docs[doc_id] = doc
        count += 1
    return docs

def dump_aggregated_jsons(input_folder, output_folder, out_fprefix, doc_id_fn=utils.default_prefix, doc_per_json=100000):
    start_time = time.time()
    json_files = [f for f in os.listdir(input_folder) if os.path.isfile(os.path.join(input_folder, f))]
    n_json_files = len(json_files)
    docs = {}
    count = 0
    for json_file in json_files:
        jpath = os.path.join(input_folder, json_file)
        #print 'reading "%s"' % (jpath)
        if count % doc_per_json == 0 and count != 0:
            logging.info('%d / %d docs processed' % (count, n_json_files))
            fname = out_fprefix + '_%s.json' % (count//doc_per_json - 1)     # 0-based index
            fpath = os.path.join(output_folder, fname)
            
            logging.info("dumping %s..." % fpath)
            logging.info("elapsed time: %f secs" % (time.time() - start_time))
            json.dump(docs, open(fpath, 'w'))
            docs = {}
        
        with open(jpath, 'r') as fr:
            doc = json.load(fr)
        doc_id = doc_id_fn(json_file)
        docs[doc_id] = doc
        count += 1

    fname = out_fprefix + '_%s.json' % (count//doc_per_json)
    fpath = os.path.join(output_folder, fname)
    logging.info("dumping %s..." % fpath)
    json.dump(partial_docs, open(fpath, 'w'))
    logging.info("total elapsed time: %f secs" % (time.time() - start_time))

def load_aggregated_jsons(input_folder):
    json_files = [f for f in os.listdir(input_folder) if os.path.isfile(os.path.join(input_folder, f))]
    count = 0
    all_docs = {}
    for json_file in json_files:
        logging.info("loading %d file: %s" % (count, json_file))
        fpath = os.path.join(input_folder, json_file)
        docs = json.load(open(fpath, 'r'))
        all_docs.update(docs)
        logging.info("n_docs = %d" % (len(all_docs)))
        count += 1
    return all_docs


