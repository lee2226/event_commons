An event extractor based on dependency trees and coreference chains.

# Dependencies

- NLTK: for word tokenization


# Installation

```
python setup.py install
```

# Preprocessing

The preprocessing, basically dependency trees and coreference resolution, should be done with Stanford CoreNLP (3.8-3.9), and the output format should JSON.

# Run Examples

```
python examples/extract.py examples/example_input.json
```

