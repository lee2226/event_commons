import os
import sys
import json
import logging
import argparse
import collections

import six
from stanfordcorenlp import StanfordCoreNLP
from nltk.stem.wordnet import WordNetLemmatizer

from event_commons import extract_events


nlp = None
props={'annotators': 'tokenize,ssplit,pos,ner,parse,dcoref', 'outputFormat':'json', 'ner.useSUTime': False}
lemmatizer = WordNetLemmatizer()


def get_arguments(argv):
    parser = argparse.ArgumentParser(description='example code for extracting events for sentences.')
    parser.add_argument('-c', '--corenlp_folder', default='/homes/lee2226/scratch2/stanford-corenlp-full-2017-06-09',
                        help='folder to the stanford corenlp parser.')
    parser.add_argument('-t', '--parse_timeout', type=int, default=60000,
                        help='parse timeout')
    args = parser.parse_args(argv)
    return args


def parse_text(text):
    global nlp, props
    parsed = None
    if nlp is None:
        nlp = StanfordCoreNLP(args.corenlp_folder, timeout=args.parse_timeout)

    server_error = False
    try:
        parsed = nlp.annotate(text, properties=props)
    except:
        logging.debug("server error: {}".format(parsed))
        server_error = True

    if not server_error:
        try:
            parsed = json.loads(parsed)
        except:
            logging.debug("json parse failed: {}".format(parsed))
    return parsed


def main():
    global lemmatizer
    text = "Jim was shot by John. He died. John was arrested by police. He will be in the jail."
    print(text+'\n\n')

    parsed = parse_text(text)
    assert len(parsed['sentences']) == 4

    events = extract_events._extract_unique_events(parsed, lemmatizer)
    for e in events:
        print(e)


if __name__ == "__main__":
    args = get_arguments(sys.argv[1:])
    main()
