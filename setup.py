#from setuptools import setup
from distutils.core import setup
from Cython.Build import cythonize

setup(name='event_commons',
      version='0.4.0',
      description='The API for extracting events',
      url='https://gitlab.com/lee2226/event_commons',
      author='I-Ta Lee @ PurdueNLP',
      author_email='lee2226@purdue.edu',
      license='MIT',
      packages=['event_commons', 'event_commons.neural_networks', 'event_commons.animacy_data'],
      package_data = {'event_commons.animacy_data': ['animate.unigrams.txt', 'inanimate.unigrams.txt']},
      ext_modules = cythonize('event_commons/extract_events.py'),
      zip_safe=False)


